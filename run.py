#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@fileName     : run.py
@description  : 
@time         : 2023年09月05日 15:01
@author       : grig
@versions     : 1.0
"""
import pytest


def run():
    pytest.main([
        # "-n","auto",
        # "-s",   # stream输出print内容，设置-s后，--html报告中不会有失败用例的详情日志
        # "-m", "smoke or merge",  # 执行标记为smoke且为merge的用例
        "--strict-markers",  # 检查是否存在未注册的标记mark
        "-d",  # 分布式执行，需安装pytest-xdist
        "--rsyncdir", "G:/src/pytest-template",  # 分布式执行，需要同步的目录，支持多个
        # "--tx","ssh=root@123.57.138.224//python=/usr/python37/bin/python3//chdir=/usr/python-script", # 分布式执行，ssh方式；指定了python编译器；指定工作目录
        "--tx", "ssh=root@123.57.138.224//python=/usr/python37/bin/python3",  # 分布式执行，ssh方式；指定了python编译器；未指定工作目录
        # "--tx", "socket=123.57.138.224:5000",  # 分布式执行，socket方式
        "--rsyncignore", "*.pyc",  # 分布式执行，不同步的文件
        "--rsyncignore", "*.md",  # 分布式执行，不同步的文件
        "pytest-template",  # 分布式执行脚本的根目录
        # "--boxed",
        "--html", "G:/src/pytest-template/report/report.html",  # 输出html报告，需安装pytest-html
        # "--alluredir","G:/src/pytest-template/report",
    ])


if __name__ == "__main__":
    run()
