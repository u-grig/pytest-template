#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@fileName     : conftest.py
@description  : 公共固件库
@time         : 2023年09月02日 12:24
@author       : grig
@versions     : 1.0
"""
import pytest


@pytest.fixture()
def func_fixture():
    print('这是func_fixture的开始')
    yield
    print('这是func_fixture的结束')


@pytest.fixture(scope='session', autouse=True)
def session_fixture():
    print('这是session_fixture的开始')
    yield
    print('这是session_fixture的结束')


@pytest.fixture(scope='module', autouse=True)
def module_fixture():
    print('这是module_fixture的开始')
    yield
    print('这是module_fixture的结束')


@pytest.fixture(scope='class')
def class_fixture():  # class范围的fixture需要在测试class的地方显式调用：@pytest.mark.usefixtures('class_fixture')
    print('这是class_fixture的开始')
    yield
    print('这是class_fixture的结束')


@pytest.fixture(params=[('Sam', 18), ('Tom', 30), ('David', 45), ('John', 12)], name='fixture_param')   # 固件参数化，可以对固件重命名，使用如：def test_func_three(rs, fixture_param):
def param(request):
    """固件参数化"""
    return request.param
