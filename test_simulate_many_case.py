#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@fileName     : test_simulate_many_case.py
@description  : 模拟如何处理用例数量多执行时间长的场景
@time         : 2023年09月04日 14:48
@author       : grig
@versions     : 1.0
"""
import random
from time import sleep

import pytest


@pytest.fixture(name='rs')
def random_sleep():
    """pytest固件，返回0~9的整数"""
    return random.choice(range(10))


def test_func_one(rs):
    """方法测试用例1"""
    sleep(rs)
    assert True


def test_func_two(rs):
    """方法测试用例2"""
    sleep(rs)
    assert True


def test_func_three(rs, fixture_param):
    """ 方法测试用例3
        使用conftest模块的参数话固件fixture_param(param重命名)
    """
    sleep(rs)
    name, age = fixture_param
    print(f'姓名：{name}，年龄：{age}')
    assert age > 18


class TestSimulateLongTime:
    """模拟执行时间较长的场景"""

    def test_case_one(self, rs):
        sleep(rs)
        assert True

    def test_case_two(self, rs):
        sleep(rs)
        assert False

    def test_case_three(self, rs):
        sleep(rs)
        assert True

    def test_case_four(self, rs):
        sleep(rs)
        assert True

    def test_case_five(self, rs):
        sleep(rs)
        assert True

    def test_case_six(self, rs):
        sleep(rs)
        assert True

    def test_case_seven(self, rs):
        sleep(rs)
        assert True

    def test_case_eight(self, rs):
        sleep(rs)
        assert True

    def test_case_nine(self, rs):
        sleep(rs)
        assert True

    def test_case_ten(self, rs):
        sleep(rs)
        assert True

    def test_case_eleven(self, rs):
        sleep(rs)
        print(f'休眠时长{rs}')
        assert True


"""
需安装pytest-xdist插件
方式一：本地多进程执行
    pytest -n X，X代表指定用于执行的cup个数，一般是服务器核数的1/2。X也可以指定为auto，即使用电脑的所有核数
    
方式二：远程分布式执行
    pytest -d --tx ssh=user_name@server_ip//python=指定服务器的python解释器路径//chdir=指定服务器的工作路径 --rsyncdir=本地需同步的目录  --tx sh=user_name@server_ip//python=指定服务器的python解释器路径//chdir=指定服务器的工作路径
    如：pytest -d --rsyncdir=./ --tx ssh=root@123.57.138.224//python=/usr/python37/bin/python3.7//chdir=/usr/python-script
    备注：使用ssh方式前，需要配置免密登录服务器
            1.windows本机生成私钥、公钥，命令行执行：ssh-keygen，提示输入密码直接回车。密钥生成的目录C:/Users/dearw/.ssh/id_rsa(私钥),C:/Users/dearw/.ssh/id_rsa.pub(公钥)
            2.将公钥拷贝到linux服务器，命令行执行：ssh-id-copy username@remote-server后，输入username的密码。拷贝到  cd ~/.ssh/authorized_keys
            3.检查是否设置成功，重新打开cmd窗口，输入：ssh username@remote-server回车，如果远程登录成功，则开通成功
"""
