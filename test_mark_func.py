#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@fileName     : test_mark_func.py
@description  : 标记函数的几种方式
@time         : 2023年09月09日 10:59
@author       : grig
@versions     : 1.0
"""
import pytest


def test_marker_func_one():
    assert True


@pytest.mark.merge
def test_marker_func_two():
    print('merge mark')
    assert True


@pytest.mark.smoke
def test_mark_func_three():
    print('smoke mark')
    assert True


def test_mark_func_four():
    assert True


@pytest.mark.merge
@pytest.mark.smoke
def test_mark_func_five():
    print('two mark')
    assert True


def test_mark_func_six():
    assert True


"""
标记执行函数的三种方式
方式一：指定函数执行，【pytest ./test_mark_func.py::test_marker_func_two】，仅执行test_marker_func_two，弊端是一次只能指定一个函数，不支持批量

方式二：模糊匹配，使用 -k 选项标识，【pytest -k marker ./test_mark_func.py】，执行test_marker_func_one和test_marker_func_two，支持批量，弊端是函数名需包含相同的模式，不怎么方便

方式三：@pytest.mark.xx 进行标记，执行时使用 -m 选项标记。需在pytest.ini配置文件中注册mark
       (1)、执行某个标记，【pytest -m smoke ./test_mark_func.py】，执行方法test_mark_func_three和test_mark_func_five
       (2)、标记支持 and or not，【pytest -m "merge and smoke" ./test_mark_func.py】,执行方法test_mark_func_five；
                               【pytest -m "merge or smoke" ./test_mark_func.py】，执行方法test_marker_func_two、test_mark_func_three、test_mark_func_five
                               【pytest -m "merge and not smoke" ./test_mark_func.py】执行方法test_marker_func_two
"""
