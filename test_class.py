#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@fileName     : test_class.py
@description  :
@time         : 2023年09月02日 14:11
@author       : grig
@versions     : 1.0
"""
import pytest


@pytest.mark.usefixtures('class_fixture')  # 显式调用class范围的fixture
class ClassOne():
    def test_class_one_1(self):
        assert True

    def test_class_one_2(self):
        assert type('name') is str


class TestClassTwo():
    def test_class_two_1(self):
        assert True

    def test_class_two_2(self):
        assert True