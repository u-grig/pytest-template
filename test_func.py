#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@fileName     : test_func.py
@description  : 
@time         : 2023年09月02日 14:06
@author       : grig
@versions     : 1.0
"""
import pytest


def test_abc():
    print(f'用例:abc')
    assert True


def test_one(func_fixture):
    print(f'用例:one')
    assert 1 == 1


# 跳过
@pytest.mark.skip(reason='跳过测试')
def test_two():
    print('two')
    assert 'a' in 'bac'


# 条件跳过
@pytest.mark.skipif(pytest.__version__ < '8', reason='版本小于7')
def test_three():
    assert 4 > 5


@pytest.mark.run(order=2)  # 第2个执行
@pytest.mark.xfail(pytest.__version__ < '8', reason='不支持的版本')
def test_four():
    assert False


# 设置执行顺序，需安装pytest-ordering
@pytest.mark.parametrize(('var', 'pwd'), [(1, 9), (2, 4), (3, 9)])
@pytest.mark.run(order=1)  # 第1个执行
def test_five(var, pwd):
    assert var + pwd < 11


# 失败重试 ，需安装pytest-rerunfailures【待探索，如果用pytest-xdist分布式执行脚本，pytest-rerunfailures报错connection refused】
# 方式一：命令行失败重试  pytest -v --reruns 2 --reruns-delay 5 test_rerun.py
# 方式二：脚本中失败重试
# @pytest.mark.flaky(reruns=2, reruns_delay=3)  # 失败重试两次，每次延时3秒执行
def test_six():
    assert 'a' in ['b', 'c']
